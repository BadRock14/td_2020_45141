import matplotlib.pyplot as plt
import pylab
import math


def floatRange(start, stop, step=None):
    if not step:
        step = 1.0
    count = 0
    while True:
        temp = float(start + count * step)
        if step > 0 and temp >= stop:
            break
        elif step < 0 and temp <= stop:
            break
        yield temp
        count += 1


class Lab1:
    def __init__(self):
        self.vFirstLeft = 0.22
        self.vFirstRight = 0
        self.vSecondLeft = 0.22
        self.vSecondRight = 0.7
        self.vThirdLeft = 1.0
        self.vThirdRight = 0.7

        self.deltaTNumeral = 1
        self.deltaTDenominator = 100

        self.evaluatedNr = []
        self.dTNumeral = 1
        self.dTDenominator = 100
        self.deltaT = self.dTNumeral / self.dTDenominator
        self.albumNumber = '45141'

        self.zeroPlace = True

        self.currentFunction = self.xFunction

        self.xStart = -10
        self.xEnd = 10

        self.evaluateAlbumNumber()

    def xFunction(self, t):
        if self.zeroPlace:
            self.zeroPlace = False
            d = (self.evaluatedNr[1] ** 2) - (4 * self.evaluatedNr[0] * self.evaluatedNr[2])
            if d > 0:
                m1 = (-self.evaluatedNr[1] - math.sqrt(d)) / (2 * self.evaluatedNr[0])
                m2 = (-self.evaluatedNr[1] + math.sqrt(d)) / (2 * self.evaluatedNr[0])
                print("Miejsca zerowe : \n{}\n{} ".format(m1, m2))
            if d == 0:
                m1 = -self.evaluatedNr[1] / (2 * self.evaluatedNr[0])
                print("Miejsca zerowe : {} ".format(m1))
            else:
                print("Brak rzeczywistych miejsc zerowych")
        return self.evaluatedNr[0] * t ** 2 + self.evaluatedNr[1] * t + self.evaluatedNr[2]

    def yFunction(self, t):
        return 2 * (self.xFunction(t)) ** 2 + 12 * math.cos(t)

    def zFunction(self, t):
        return math.sin(2 * math.pi * 7 * t) * self.xFunction(t) - 0.2 * math.log10(abs(self.yFunction(t)) + math.pi)

    def uFunction(self, t):
        return math.sqrt(abs(self.yFunction(t) * self.yFunction(t) * self.zFunction(t))) - 1.8 * math.sin(
            0.4 * t * self.zFunction(t) * self.xFunction(t))

    def vFunction(self, t):
        if float(self.vFirstLeft) > t >= float(self.vFirstRight):
            return (1 - 7 * t) * math.sin((2 * math.pi * t * 10) / (t + 0.04))
        if float(self.vSecondLeft) <= t < float(self.vSecondRight):
            return 0.63 * t * math.sin(125 * t)
        if float(self.vThirdLeft) >= t >= float(self.vThirdRight):
            return math.pow(t, -0.662) + 0.77 * math.sin(8 * t)

    def p1Function(self, t):
        return sum((math.cos(12 * t * n * 2) + math.cos(16 * t * n)) / (n * 2) for n in
                   range(1, 2))

    def p2Function(self, t):
        return sum((math.cos(12 * t * n * 2) + math.cos(16 * t * n)) / (n * 2) for n in
                   range(1, 4))

    def pFunction(self, t):
        string = '{}{}'.format(self.evaluatedNr[0], self.evaluatedNr[1])
        return sum((math.cos(12 * t * n * 2) + math.cos(16 * t * n)) / (n * 2) for n in
                   range(1, int(string)))

    def evaluateAlbumNumber(self):
        for i, number in enumerate(self.albumNumber[::-1]):
            self.evaluatedNr.append(int(number) if int(number) or i == 2 else 1)
        for i in range(0, 6 - len(self.evaluatedNr)):
            self.evaluatedNr.append(0)

    def calc(self, function):
        res = {}
        for t in floatRange(int(self.xStart), int(self.xEnd), self.deltaT):
            res[t] = function(t)

        return res

    def plotXFunction(self):
        plt.title("Signal x(t)")
        plt.ylabel("x(t)")
        self.currentFunction = self.xFunction
        self.mainPlot()

    def plotYFunction(self):
        plt.title("Signal y(t)")
        plt.ylabel("y(t)")
        self.currentFunction = self.yFunction
        self.mainPlot()

    def plotZFunction(self):
        plt.title("Signal z(t)")
        plt.ylabel("z(t)")
        self.currentFunction = self.zFunction
        self.mainPlot()

    def plotUFunction(self):
        plt.title("Signal u(t)")
        plt.ylabel("u(t)")
        self.currentFunction = self.uFunction
        self.mainPlot()

    def plotVFunction(self):
        plt.title("Signal v(t)")
        plt.ylabel("v(t)")
        self.currentFunction = self.vFunction
        self.mainPlot()

    def plotPFunction(self):
        plt.title("Signal p(t) dla N ={}{}".format(self.evaluatedNr[0], self.evaluatedNr[1]))
        plt.ylabel("p(t)")
        self.currentFunction = self.pFunction
        self.mainPlot()

    def plotP1Function(self):
        plt.title("Signal p(t) dla N = 2")
        plt.ylabel("p(t)")
        self.currentFunction = self.p1Function
        self.mainPlot()

    def plotP2Function(self):
        plt.title("Signal p(t) dla N = 4")
        plt.ylabel("p(t)")
        self.currentFunction = self.p2Function
        self.mainPlot()

    def mainPlot(self):
        if int(self.xStart) > int(self.xEnd):
            print('Zly zakres osi X.\nSprobuj ponownie')
            return

        results = self.calc(self.currentFunction)

        x = list(results.keys())
        y = list(results.values())
        pylab.plot(x, y)
        pylab.show()

    def deltaDenominatorChanged(self):
        self.deltaTDenominator = 22050
        self.dTDenominator = int(self.deltaTDenominator)
        self.deltaT = self.dTNumeral / self.dTDenominator

    def changeRange(self):
        self.xStart = 0
        self.xEnd = 1


if __name__ == '__main__':
    main = Lab1()
    main.plotXFunction()

    main.deltaDenominatorChanged()
    main.changeRange()

    main.plotYFunction()
    main.plotZFunction()
    main.plotUFunction()
    main.plotVFunction()
    main.plotPFunction()
    main.plotP1Function()
    main.plotP2Function()
