import matplotlib.pyplot as plt
import pylab
import math


def floatRange(start, stop, step=None):
    if not step:
        step = 1.0
    count = 0
    while True:
        temp = float(start + count * step)
        if step > 0 and temp >= stop:
            break
        elif step < 0 and temp <= stop:
            break
        yield temp
        count += 1


class Lab1:
    def __init__(self):
        self.deltaTNumeral = 1
        self.deltaTDenominator = 100

        self.evaluatedNr = []
        self.albumNumber = '45141'

        self.evaluateAlbumNumber()

        self.dTNumeral = 1
        self.dTDenominator = self.evaluatedNr[1] ** 3
        self.deltaT = self.dTNumeral / self.dTDenominator

        self.resolution = 1

        self.currentFunction = self.sFunction

        self.xStart = 0
        self.xEnd = self.evaluatedNr[0]

    def sFunction(self, t):
        return 1.0 * math.sin(2 * math.pi * self.evaluatedNr[1] * t + self.evaluatedNr[2] * math.pi)

    def sQuantizedFunction(self, t):
        return self.__quantize(self.sFunction(t))

    def __quantize(self, t):
        return ((-t / -0.5 if t < 0 else t / 0.5) / 2 * self.resolution) + self.resolution

    def evaluateAlbumNumber(self):
        for i, number in enumerate(self.albumNumber[::-1]):
            self.evaluatedNr.append(int(number) if int(number) or i == 2 else 1)
        for i in range(0, 6 - len(self.evaluatedNr)):
            self.evaluatedNr.append(0)

    def calc(self, function):
        res = {}
        for t in floatRange(int(self.xStart), int(self.xEnd), self.deltaT):
            res[t] = function(t)

        return res

    def plotSFunction(self):
        self.resolution = 1
        self.deltaT = self.dTNumeral / self.dTDenominator
        plt.title("Signal s(t)")
        plt.ylabel("s(t)")
        self.currentFunction = self.sFunction
        self.mainPlot()

    def plotSQuantizedFunction(self):
        self.resolution = 2 ** 15
        self.deltaT = self.dTNumeral / self.dTDenominator
        plt.title("Qunatized Signal s(t)")
        plt.ylabel("s(t)")
        self.currentFunction = self.sQuantizedFunction
        self.mainPlot()

    def plotSQuantizedDividedByHalfFunction(self):
        self.resolution = 2 ** 7
        self.deltaT /= 2
        plt.title("Qunatized Signal s(t)")
        plt.ylabel("s(t)")
        self.currentFunction = self.sQuantizedFunction
        self.mainPlot()

    def mainPlot(self):
        if int(self.xStart) > int(self.xEnd):
            print('Zly zakres osi X.\nSprobuj ponownie')
            return

        results = self.calc(self.currentFunction)

        x = list(results.keys())
        y = list(results.values())
        pylab.plot(x, y)
        pylab.show()


if __name__ == '__main__':
    main = Lab1()
    main.plotSFunction()
    main.plotSQuantizedFunction()
    main.plotSQuantizedDividedByHalfFunction()
